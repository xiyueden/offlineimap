#!/usr/bin/env python3
from subprocess import check_output


def query_auth_source(account, imap_url, key):
    if imap_url is None:
        imap_url = account

    cmd = ["/usr/bin/emacs", "--script",
           "~/.config/offlineimap/auth-source-query.el",
           account, imap_url, key]
    result = check_output(cmd)

    if key != "password":
        result = result.decode('utf-8')
    return result


def get_password(account, imap_url=None):
    return query_auth_source(account, imap_url, "password")


def get_request_url(account, imap_url=None):
    return query_auth_source(account, imap_url, "token_url")


def get_client_id(account, imap_url=None):
    return query_auth_source(account, imap_url, "client_id")


def get_client_secret(account, imap_url=None):
    return query_auth_source(account, imap_url, "client_secret")


def get_refresh_token(account, imap_url=None):
    return query_auth_source(account, imap_url, "refresh_token")

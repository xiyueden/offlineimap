#!/usr/bin/env emacs --script

(require 'auth-source)
(add-to-list 'load-path (concat user-emacs-directory "elpa/oauth2-0.17"))
(require 'oauth2)

(setq auth-sources '("~/.authinfo.json.gpg" "~/.authinfo.gpg"))
(setq oauth2-plstore (concat user-emacs-directory "oauth2.plstore"))

(let* ((user (elt argv 0))
       (host (elt argv 1))
       (key (elt argv 2))
       (hosts `(,user ,host))
       (port "imaps")
       (query-result (car (auth-source-search :host hosts
                                              :user user
                                              :port port
                                              :create nil
                                              :max 1)))
       result)
  (when query-result
    (cond
     ((string= key "password")
      (setq result (auth-info-password query-result)))
     ((string= key "token_url")
      (setq result (plist-get query-result :token-url)))
     ((string= key "client_id")
      (setq result (plist-get query-result :client-id)))
     ((string= key "client_secret")
      (setq result (plist-get query-result :client-secret)))
     ((string= key "refresh_token")
      (when-let* ((auth-url (plist-get query-result :auth-url))
                  (token-url (plist-get query-result :token-url))
                  (scope (plist-get query-result :scope))
                  (client-id (plist-get query-result :client-id))
                  (oauth2-plstore-id (oauth2-compute-id auth-url token-url scope
                                                        client-id))
                  (plstore (plstore-open oauth2-plstore))
                  (plstore-data (cdr (plstore-get plstore oauth2-plstore-id))))
        (message "oauth2-plstore-id: %s" oauth2-plstore-id)
        (setq result (plist-get plstore-data :refresh-token))))
     (t
      (error "Unknown field: %s" key)))
    (princ (concat result "\n"))))
